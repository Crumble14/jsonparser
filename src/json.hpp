#ifndef JSON_HPP
# define JSON_HPP

# include <cstring>
# include <memory>
# include <string>
# include <unordered_map>
# include <vector>

# include <iostream> // TODO remove

namespace JSON
{
	class Element
	{
		public:
		virtual bool is_value() const = 0;
		virtual bool is_object() const = 0;
		virtual bool is_array() const = 0;

		virtual std::string dump(const bool pretty,
			const size_t level = 0) const = 0;
	};

	using Element_t = std::shared_ptr<Element>;

	enum ValueType : uint8_t
	{
		NULL_JT = 0,
		INTEGER_JT,
		FLOAT_JT,
		BOOLEAN_JT,
		STRING_JT
	};

	class Value : public Element
	{
		public:
		Value() = default;

		template<typename T>
		inline Value(const T value)
		{
			operator=(value);
		}

		inline Value(Value &value)
			: data{value.data}, type{value.type}, copied{value.copied}
		{
			value.copied = true;
		}

		inline ~Value()
		{
			if(!copied)
				null();
		}

		inline void operator=(Value &value)
		{
			data = value.data;
			type = value.type;
			value.copied = true;
		}

		inline bool is_value() const override
		{
			return true;
		}

		inline bool is_object() const override
		{
			return false;
		}

		inline bool is_array() const override
		{
			return false;
		}

		inline bool is_null() const
		{
			return (data == nullptr);
		}

		inline operator Element_t()
		{
			return Element_t(new Value(*this));
		}

		template<typename T>
		inline operator T() const
		{
			if(is_null())
				return 0;
			return *reinterpret_cast<const T*>(data);
		}

		template<typename T>
		inline void operator=(const T v)
		{
			store(&v, sizeof(v));
		}

		void null();

		std::string dump(const bool pretty, const size_t level = 0) const;

		private:
		void* data = nullptr;
		ValueType type = NULL_JT;

		bool copied = false;

		void store(const void* ptr, const size_t size);
	};

	class Object : public Element
	{
		public:
		inline bool is_value() const override
		{
			return false;
		}

		inline bool is_object() const override
		{
			return true;
		}

		inline bool is_array() const override
		{
			return false;
		}

		inline operator Element_t()
		{
			return Element_t(new Object(*this));
		}

		inline Element_t &operator[](const std::string key)
		{
			return elements[key];
		}

		inline const Element_t &operator[](const std::string key) const
		{
			const auto i = elements.find(key);

			if(i == elements.cend())
				throw std::invalid_argument("Key not found!");
			return i->second;
		}

		inline size_t size() const
		{
			return elements.size();
		}

		inline const std::unordered_map<std::string, Element_t> &map() const
		{
			return elements;
		}

		std::string dump(const bool pretty, const size_t level = 0) const;

		private:
		std::unordered_map<std::string, Element_t> elements;
	};

	class Array : public Element
	{
		public:
		inline bool is_value() const override
		{
			return false;
		}

		inline bool is_object() const override
		{
			return false;
		}

		inline bool is_array() const override
		{
			return true;
		}

		inline operator Element_t()
		{
			return Element_t(new Array(*this));
		}

		inline Element_t &operator[](const size_t n)
		{
			return elements[n];
		}

		inline const Element_t &operator[](const size_t n) const
		{
			return elements[n];
		}

		inline void push(const Element_t &e)
		{
			elements.push_back(e);
		}

		inline void pop()
		{
			elements.pop_back();
		}

		inline size_t size() const
		{
			return elements.size();
		}

		inline const std::vector<Element_t>& vec() const
		{
			return elements;
		}

		std::string dump(const bool pretty, const size_t level = 0) const;

		private:
		std::vector<Element_t> elements;
	};

	std::shared_ptr<Element> parse(const char *&str);

	inline std::shared_ptr<Element> parse(const std::string &str)
	{
		const char *s = str.c_str();
		return parse(s);
	}

	void expect(const char c);
	void ueod();

	void skip_spaces(const char *&str);
	bool str_starts_with(const char *haystack, const char *needle);

	bool is_null(const char *str);
	bool is_integer(const char *str);
	bool is_float(const char *str);
	bool is_boolean(const char *str);
	bool is_string(const char *str);

	int get_integer(const char *&str);
	float get_float(const char *&str);
	bool get_boolean(const char *&str);
	std::string get_string(const char *&str);

	inline void append_tab(std::string &str, const size_t n)
	{
		str.reserve(str.size() + n);
		for(size_t i = 0; i < n; ++i)
			str += '\t';
	}
}

#include <value.tpp>

#endif
