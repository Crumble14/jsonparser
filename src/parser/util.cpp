#include <json.hpp>

using namespace JSON;

void JSON::expect(const char c)
{
	throw std::invalid_argument(std::string("Expected `") + c + "`!");
}

void JSON::ueod()
{
	throw std::invalid_argument("Unexpected end of data!");
}

void JSON::skip_spaces(const char *&str)
{
	if(!str)
		return;
	while(isspace(*str))
		++str;
}

bool JSON::str_starts_with(const char *haystack, const char *needle)
{
	if(!haystack || !needle)
		return false;
	while(*haystack && *needle && *haystack == *needle)
	{
		++haystack;
		++needle;
	}
	return (*needle == '\0');
}

bool JSON::is_null(const char *str)
{
	skip_spaces(str);
	return str_starts_with(str, "null");
}

bool JSON::is_integer(const char *str)
{
	if(!str)
		return false;
	skip_spaces(str);
	if(*str == '+' || *str == '-')
		++str;
	return (*str >= '0' && *str <= '9');
}

bool JSON::is_float(const char *str)
{
	if(!str)
		return false;
	skip_spaces(str);
	if(*str == '+' || *str == '-')
		++str;
	while(*str && *str >= '0' && *str <= '9' && *str != '.')
		++str;
	if(*str == '\0')
		return false;
	return ((*str >= '0' && *str <= '9') || *str == '.');
}

bool JSON::is_boolean(const char *str)
{
	skip_spaces(str);
	return (str_starts_with(str, "true") || str_starts_with(str, "false"));
}

bool JSON::is_string(const char *str)
{
	if(!str)
		return false;
	skip_spaces(str);
	if(*str != '"')
		return false;
	++str;
	while(*str && *str != '"')
	{
		if(str[0] == '\\' && str[1])
			++str;
		++str;
	}
	return (*str == '"');
}

int JSON::get_integer(const char *&str)
{
	int n = 0;
	bool neg;

	if(!str)
		return 0;
	skip_spaces(str);
	neg = (*str == '-');
	if(*str == '+' || *str == '-')
		++str;
	if(*str == '\0')
		ueod();
	while(*str >= '0' && *str <= '9')
		n = (n * 10) + (*(str++) - '0');
	return (neg ? -n : n);
}

float JSON::get_float(const char *&str)
{
	float n = 0;
	bool neg;
	float m = 1;

	if(!str)
		return 0;
	skip_spaces(str);
	neg = (*str == '-');
	if(*str == '+' || *str == '-')
		++str;
	if(*str == '\0')
		ueod();
	while(*str >= '0' && *str <= '9')
		n *= (n * 10) + (*(str++) - '0');
	if(*str == '.') {
		++str;
		while(*str >= '0' && *str <= '9')
		{
			m *= 0.1;
			n += (*str - '0') * m;
			++str;
		}
	}
	return (neg ? -n : n);
}

bool JSON::get_boolean(const char *&str)
{
	if(!str)
		return 0;
	skip_spaces(str);
	if(str_starts_with(str, "true"))
	{
		str += 4;
		return true;
	}
	else if(str_starts_with(str, "false"))
		str += 5;
	return false;
}

static char translate_backslash(const char c)
{
	switch(c)
	{
		case 'n': return '\n';
		case 'r': return '\r';
		case 't': return '\t';
		case 'b': return '\b';
		case 'f': return '\f';
		case 'v': return '\v';
		default: return c;
	}
}

std::string JSON::get_string(const char *&str)
{
	std::string s;

	if(!str)
		return s;
	skip_spaces(str);
	if(*str != '"')
		expect('"');
	++str;
	while(*str && *str != '"')
	{
		if(*str == '\\')
		{
			++str;
			s += translate_backslash(*str);
		}
		else
			s += *str;
		++str;
	}
	if(*str == '\0')
		ueod();
	++str;
	return s;
}
