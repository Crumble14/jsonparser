#include <json.hpp>

using namespace JSON;

// TODO Convert binary to escape codes?
static inline std::string quote(const std::string&& str)
{
	std::string s;
	size_t i;

	s.reserve(str.size() + 2);
	s += '"';
	for(i = 0; i < str.size(); ++i) {
		const auto &c = str[i];

		if(c == '"')
			s += '\\';
		s += c;
	}
	s += '"';
	return s;
}

void Value::null()
{
	if(data)
		free(data);
	data = nullptr;
	type = NULL_JT;
}

void Value::store(const void* ptr, const size_t size)
{
	null();
	if(!(data = malloc(size)))
		throw std::bad_alloc();
	memcpy(data, ptr, size);
}

std::string Value::dump(const bool, const size_t) const
{
	switch(type)
	{
		case NULL_JT: return "null";
		case INTEGER_JT: return std::to_string(operator int());
		case FLOAT_JT: return std::to_string(operator float());
		case BOOLEAN_JT: return (operator bool() ? "true" : "false");
		case STRING_JT: return quote(operator const char *());
	}
	throw std::runtime_error("Unknown value type!");
}
